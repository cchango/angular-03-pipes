import { Component } from '@angular/core';
import { promise } from 'protractor';
import { resolve } from 'dns';
import { reject } from 'q';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pipes';
  nombre = 'Carlos Adrian';
  nombre2 = 'carsLOS Adrian cHango loZano';
  arreglo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  PI = Math.PI;
  a = 0.234;
  salario = 1234.5;
 iso = {"Id":0,"Atm_Id":6,"Init_Download":false,"NDC_States_idx":29,"tables_loading":false,"tran_in_prog":false,"in_supervisor":false,"states_loaded":true,"PPK_loaded":true,"Fitness_requested":true,"Sensors_requested":true,"Supplies_requested":true,"Counters_requested":true,"EJ_ENABLED":true,"MACKey_loaded":true,"MACSelection_loaded":false,"mac_flag":false,"date_loaded":true,"ATM_Manufacturer":"","CashInNoteConfigure_loaded":false,"CashIN_Note_Definition":"","LAST_Command_Send":"173","open_flag":true,"Acq_Inst_Id_Code_32":"627247","NDC_last_state_loaded":"","Tran_Seq_No":43,"Waiting_For_Interactive":false,"Message_Coordinator":"","ATM_Waits_CAM_data":true,"ATM_Waits_CAM_91":true,"TR_Amount_K":0.0,"TR_T2_H":"","ACT_FROM":"","ACT_TO":"","Receipt_Printer_OK":true,"Receipt_Paper_OK":true,"ONUS":false,"ProcCode":0,"Tran_Curr_Code":840,"Step":49,"Session":"","PPK_Criptogram":"11464FF52D1D6EE35EF86C9B8F1B334CFA","MAC_Criptogram":"","Language":"","Receipt":"","Next_State":"","Scr_Ask_From":"","Scr_Ask_To":"","Scr_Tran_Resp":"","Account1":"","Account2":"","Account3":"","Account4":"","Account5":"","Account6":"","Account7":"","Account8":"","ActiveInteractive":"","PendingInteractives":null,"ProcCodeBuild":"","CDMCounter1":199,"CDMCounter2":299,"CDMCounter3":0,"CDMCounter4":0,"NotesDispensed":"00000000","CDMFitness":"00000","CDMSensors":"00000","StateAskAmount":"","LocalTranTime":163512,"LocalTranDate":928,"ExpectedKVC":"2D2D3F","DE_CardIssuer_AuthData":"","ipAddress":"","TranAccount1":"4501416864","TranAccount2":"","EJ_Buffer":"","EJ_LastCharProcessed":0,"Terminal_Id":null,"DynamicId":"","CDMSupplies":"12200","Fitness":"A0\u001dB0\u001dC0\u001dD0\u001dE00000\u001dG0\u001dH0\u001dL0\u001da0","Supplies":"D1\u001dE12200\u001dG1111\u001dH111","Scope":"0381df79-54d3-4923-8070-2bb9dd90f171","De055_EMV":"5F2A0208405F340101820218009A031909289C01319F02060000000000009F03060000000000009F1A0202189F1E0841424344313233349F10200FA501A131C0000000000000000000000F0100000000000000000000000000009F2608F74DE081DE9A76069F2701809F33036040009F34033F00019F36020FC09F3704C6523F369505808080800057136272477385710047D20036018392407794054F9F0D05B050BC88009F02060000000000009F03060000000000009F1A020218950580808080005F2A0208409A031909289C01319F3704C6523F369F350114","De052_Pinblock":"=35125043:8672<6","OPCODE":"C     CB","CurrentTransactionKey":"2^NDC^0200^000043^0928213512^QA200199","ExtraData":{}};
 valorDePromesa = new Promise ( ( resolve, reject) => { setTimeout ( () => resolve('Llego la data'), 350); })
 fecha = new Date();
 video = 'jM-hPfakL2A';
}
