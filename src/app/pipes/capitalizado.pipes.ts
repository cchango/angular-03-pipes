import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'capitalizado'})
export class CapitalizadoPipe implements PipeTransform {
    transform(value: string, todas: boolean = true ): string {
        if (todas) {
            value = value.toUpperCase();
        } else {
            value = value.toLowerCase();
        }
        return  value;
    }
}
