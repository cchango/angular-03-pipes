import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contrasena'
})
export class ContrasenaPipe implements PipeTransform {

  transform(value: string,  activar: boolean = true): string {
    if (activar) {
      const espacio = ' ';
      let salida = '';
      const arreglo: string[] = value.split(' ');
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < arreglo.length ; i++) {
        // tslint:disable-next-line: prefer-for-of
        for (let j = 0; j < arreglo[i].length; j++) {
          salida += '*';
        }
        salida += espacio;
      }
      return salida;
    } else {
       value = value.toUpperCase();
       return value;
    }
  }
}
